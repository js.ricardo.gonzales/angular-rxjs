import { Component } from '@angular/core';
import { 
  asyncScheduler, 
  Observable, 
  of,
  from, 
  fromEvent,
  interval,
  timer,
  Subject,
  BehaviorSubject,
  combineLatest, 
  merge
} from "rxjs";
import {
  switchMap,
  publish,
  shareReplay,
  map, filter, first, take, scan, tap,
  debounceTime, throttleTime, bufferCount,
  delay,
  catchError,
  takeLast,
  takeUntil,
  takeWhile
} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-rxjs';
}

// Basic - basic obs
const basic = new Observable((observer) => {
  observer.next('vak 1');
  observer.next('vak 2');
  observer.complete();
  observer.next('vak 3'); 
});
basic.subscribe(console.log);

// of - simple string
const hello = of('halo');
hello.subscribe(console.log);

// from - Its gonna split every character 
//        arrays or strings or objects
const element = "world";
const world = from(element, asyncScheduler);
world.subscribe(console.log);

// event
const event = fromEvent(document, "click");
event.subscribe(console.log);

// periodic
const periodic = interval(500);
const intervalObserver = periodic.subscribe(console.log);
setTimeout(() => {
  intervalObserver.unsubscribe();
}, 3000);

// cold & hot

// cold observable
// output 1 ! ouptue 2
const cold = new Observable((obs) => {
  obs.next(Math.random());
});
cold.subscribe(console.log); // output 1
cold.subscribe(console.log); // output 2

// to get the same output value we should conver
// the cold obs to hot
const hot = cold.pipe(shareReplay(1));
hot.subscribe(console.log);
hot.subscribe(console.log);

// alternative way
//const hot = cold.pipe(publish());
//hot.subscribe(console.log);
//hot.subscribe(console.log);
//hot.connect();

// Subject
const subject = new Subject();
subject.subscribe(console.log);
subject.next('beta');
subject.next('teta');
subject.subscribe(console.log);

// Subject behaviorSubject
const bs = new BehaviorSubject("India");
bs.subscribe(console.log);
bs.next("Peru")
bs.subscribe(console.log);

// Operators
const source = from([1,2,3,4,5]);
const modified = source.pipe(
  map(item => item * 1),
  scan((acc, val) => acc + val),
  filter(v => v < 10),
  take(2)
);
modified.subscribe(console.log);


// TAP
// Can do thing bettwen processes
const simpleSource = of('Jeff');
const tapped = simpleSource.pipe(
  tap(console.log),
  map(v => v.toUpperCase()),
  tap(console.log)
)
tapped.subscribe();


// BACKPRESSURE
const event2 = fromEvent(document, 'mousemove').pipe(map(_ => Math.random() + '💩'));
// event.subscribe(console.log)

const debounced = event2.pipe(debounceTime(1000)); // take the value every second
// debounced.subscribe(console.log);

const throttled = event2.pipe(throttleTime(1000));
//throttled.subscribe(console.log);

const buffered = event2.pipe(bufferCount(20)); // take last 20
// buffered.subscribe(console.log);


// SWTICH MAP

// Function to simulate database call
const fetchOrders = async(userId: any) => {
  return `${userId}'s order data`
}

const user$ = of({uid: Math.random()});

const orders$ = user$.pipe(
  switchMap(user => fetchOrders(user.uid))
)

user$.subscribe(console.log);
orders$.subscribe(console.log);


// COMBINE

const randoAsync = new Observable((o) => o.next(`${Math.random()}oijoij`));
const delayed = randoAsync.pipe(delay(6000));

// combineLast
const combo = combineLatest([
  delayed,
  randoAsync,
  randoAsync,
  randoAsync
]);
combo.subscribe(console.log);

// merge
const merged = merge([
  delayed,
  randoAsync,
  randoAsync,
  randoAsync
]);
merged.subscribe(console.log);

// CATCH
const sub = new Subject();

sub.pipe(
  catchError(err => of('Something wrong here'))
).subscribe ({
  next: val => console.log(val),
  error: val => console.log(val)
})

sub.next('good');
sub.error('broken');


// UNSUBSCRIBE

const source2 = interval(100);

const example1 = source2.pipe(
  takeWhile(v => v <=20)
)
example1.subscribe(console.log);

const example2 = source2.pipe(
  takeUntil(timer(8000))
)
example2.subscribe(console.log);
